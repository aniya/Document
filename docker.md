# Docker

## Docker安装

```powershell
#!/bin/bash
# 移除掉旧的版本
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine

# 删除所有旧的数据
sudo rm -rf /var/lib/docker

#  安装依赖包
sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

# 添加源，使用了阿里云镜像
sudo yum-config-manager \
    --add-repo \
    http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

# 配置缓存
sudo yum makecache fast

# 安装最新稳定版本的docker
sudo yum install -y docker-ce

# 配置镜像加速器
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["http://hub-mirror.c.163.com"]
}
EOF

# 启动docker引擎并设置开机启动
sudo systemctl start docker
sudo systemctl enable docker
```

## docker常用命令

```shell
systemctl start docker  # 启动docker服务

systemctl stop docker  # 停止docker服务

systemctl restart docker  # 重启docker服务

docker build 构建镜像
docker images 查看镜像
docker rmi 删除镜像
docker push 推送镜像到服务
docker pull 从服务拉去镜像
docker save 保存镜像为一个压缩包
docker load -i 文件名称 加载压缩包为镜像
docker stop 停止镜像
docker start 启动镜像
```

## docker 安装数据库

```shell
docker run -p 3306:3306 --name MYSQL8.0 -e MYSQL_ROOT_PASSWORD=123456 -d mysql:latest
#密码可以自己设置
docker exec -it MYSQL8.0 bash
    mysql -uroot -p123456
alter user 'root'@'%' identified by '123456' password expire never;
alter user 'root'@'%' identified with mysql_native_password by '123456';
flush privileges;
```

-p 3306:3306：将容器内的3306端口映射到实体机3306端口

--name MYSQL8.0：给这个容器取一个容器记住的名字

-e MYSQL_ROOT_PASSWORD=123456：docker的MySQL默认的root密码是随机的，这是改一下默认的root用户密码

-d mysql:latest：在后台运行mysql:latest镜像产生的容器。

```shell
docker pull mysql:8.0.20
docker run -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=123456  -d mysql:8.0.20
docker ps -a
docker cp  mysql:/etc/mysql /mnt/sda1/mysql8.0.20
docker stop mysql
docker rm mysql


#!/bin/sh
docker run \
-p 3306:3306 \
--name mysql \
--privileged=true \
--restart unless-stopped \
-v /mydata/mysql8.0/mysql:/etc/mysql \
-v /mydata/mysql8.0/logs:/logs \
-v /mydata/mysql8.0/data:/var/lib/mysql \
-v /etc/localtime:/etc/localtime \
-e MYSQL_ROOT_PASSWORD=123456 \
-d mysql:8.0.20


```



## docker安装redis

```shell
docker pull redis
liunx 下redis.conf文件位置： /home/redis/myredis/redis.conf
liunx 下redis的data文件位置 ： /home/redis/myredis/data
wget http://download.redis.io/redis-stable/redis.conf

docker run --restart=always --log-opt max-size=100m --log-opt max-file=2 -p 6379:6379 --name redis -v /home/redis/myredis.conf:/etc/redis/redis.conf -v /home/redis/data:/data -d redis redis-server /etc/redis/redis.conf  --appendonly yes  --requirepass 123456
```

> –restart=always 总是开机启动
> –log是日志方面的
> -p 6379:6379 将6379端口挂载出去
> –name 给这个容器取一个名字
> -v 数据卷挂载
> /home/redis/myredis/myredis.conf:/etc/redis/redis.conf 这里是将 liunx 路径下的myredis.conf 和redis下的redis.conf 挂载在一起。
> /home/redis/myredis/data:/data 这个同上
> -d redis 表示后台启动redis
> redis-server /etc/redis/redis.conf 以配置文件启动redis，加载容器内的conf文件，最终找到的是挂载的目录 /etc/redis/redis.conf 也就是liunx下的/home/redis/myredis/myredis.conf
> –appendonly yes 开启redis 持久化

## docker安装rabbitmq

```shell
docker run \
 -e RABBITMQ_DEFAULT_USER=root \
 -e RABBITMQ_DEFAULT_PASS=ctw123 \
 --name rabbitmq \
 --hostname mq1 \
 -p 15672:15672 \
 -p 5672:5672 \
 -d \
 rabbitmq:3-management
```

## docker安装nacos

```
docker pull nacos/nacos-server
docker run --name nacos-quick -e MODE=standalone -p 8848:8848 -d nacos/nacos-server
//不设置配置文件
docker run -d --name nacos -p 8848:8848  -p 9848:9848 -p 9849:9849 --privileged=true -e JVM_XMS=256m -e JVM_XMX=256m -e MODE=standalone -v /mydata/nacos/logs/:/home/nacos/logs -v /mydata/nacos/conf/:/home/nacos/conf/ --restart=always nacos/nacos-server

//设置配置文件
//需要设置数据库可以本地连接
docker run --name nacos -d \
-p 8848:8848 \
--privileged=true \
-e JVM_XMS=256m \
-e JVM_XMX=256m \
-e MODE=standalone \
-e TIME_ZONE='Asia/Shanghai' \
-v /mydata/nacos/logs:/home/nacos/logs \
-v /mydata/nacos/conf/application.properties:/home/nacos/conf/application.properties \
nacos/nacos-server:1.4.1
```

```shell
docker run -d -p 8848:8848 -e MODE=standalone -e PREFER_HOST_MODE=hostname -e JVM_XMS=256m -e JVM_XMX=256m -v /mydata/nacos/init.d/custom.properties:/home/nacos/init.d/custom.properties -v /mydata/nacos/logs:/home/nacos/logs --restart always --name nacos nacos/nacos-server:2.1.0


```



## docker安装elasticsearch

```shell
 #编写配置文件
 echo "http.host: 0.0.0.0">> /mydata/elasticsearch/config/elasticsearch.yml
 
 chmod -R 777 /mydata/elasticsearch/ 保证权限
docker run --name elasticsearch -p 9200:9200 -p 9300:9300 \
-e "discovery.type=single-node" \
-e ES_JAVA_OPTS="-Xms64m -Xmx512m" \
-v /mydata/elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-v /mydata/elasticsearch/data:/usr/share/elasticsearch/data \
-v /mydata/elasticsearch/plugins:/usr/share/elasticsearch/plugins \
-d elasticsearch:7.4.2
```

## docker安装kibaba

```
docker run --name kibana -e ELASTICSEARCH_HOSTS=http://192.168.33.10:9200/ -p 5601:5601 -d kibana:7.4.2
```



# Docker 遇到的问题

## nacos连不上mysql数据库

* mysql8.0的数据库和5.X的不同，需要加上serverTimezone=UTC
* mysql 的host 不是127.0.0.1 而是 别的
* docker的host 和主机的host 不同
