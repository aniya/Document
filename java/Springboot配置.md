# Springboot配置

## 统一异常处理

> 使用`@RestControllerAdvice`注解

```java
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public String handleException(Exception e) {
        if (e instanceof ArithmeticException) {
            return "data error";
        }
        if (e instanceof Exception) {
            return "service error";
        }
        retur null;
    }
}
```

## 返回日期格式化

```yaml
spring:
  jackson:
    date-format: yyyy-MM-dd HH:mm:ss
    time-zone: GMT+8
 
```

## JSON格式化LocalDateTime

```java
@Configuration
public class WebConfig {
     @Bean
     public Jackson2ObjectMapperBuilderCustomizer customizeLocalDateTimeFormat() {
         return jacksonObjectMapperBuilder -> {
             DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
             LocalDateTimeDeserializer deserializer = new LocalDateTimeDeserializer(formatter);
             LocalDateTimeSerializer serializer = new LocalDateTimeSerializer(formatter);
             jacksonObjectMapperBuilder.serializerByType(LocalDateTime.class, serializer);
             jacksonObjectMapperBuilder.deserializerByType(LocalDateTime.class, deserializer);
         };
     }
}

```

> 如果想要不同的格式@JsonFormat( pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")

## 手动提交事务

```java
	@Resource
    private DataSourceTransactionManager dataSourceTransactionManager;
    @Resource
    private TransactionDefinition transactionDefinition;


	TransactionStatus transaction = dataSourceTransactionManager.getTransaction(transactionDefinition);
//            手动提交事务
dataSourceTransactionManager.commit(transaction);
//  		  回滚事务
	dataSourceTransactionManager.rollback(transaction);
```

