# 依赖

```xml
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>easyexcel</artifactId>
            <version>3.2.1</version>
        </dependency>
```

# 导出数据到Excel

```java
    @Override
    public void ExportProductAnalysis(HttpServletResponse response) {
        try {
//        设置导出格式
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("utf-8");
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
//        获取数据
            List<ProductStatistical> productStatisticals = baseMapper.selectList(null);
            List<ProductStatisticalExport> exportList = productStatisticals.stream()
                    .map(item -> BeanUtil.copyProperties(item, ProductStatisticalExport.class))
                    .collect(Collectors.toList());
            //  写数据
            EasyExcel.write(response.getOutputStream())
                    .head(ProductStatisticalExport.class)
                    .excelType(ExcelTypeEnum.XLS)
                    .sheet(fileName)
                    .doWrite(exportList);
        } catch (IOException e) {
            throw ErrorCodes.EXPORT_ERROR.toException();
        }

    }
```



