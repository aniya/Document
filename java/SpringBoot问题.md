## 1.Springboot事务和锁冲突

> **因为mysql的事务默认的隔离级别是可重复读，所以会可能会发生 已经解锁但是事务没有提交的情况，那么读取的数据还是旧的数据**
>
> 导致加锁失败

解决方法 ：

一，将事务放在controller层，或者使用注解

二，手动提交事务

```java
	@Resource
    private DataSourceTransactionManager dataSourceTransactionManager;
    @Resource
    private TransactionDefinition transactionDefinition;


	TransactionStatus transaction = dataSourceTransactionManager.getTransaction(transactionDefinition);
//            手动提交事务
dataSourceTransactionManager.commit(transaction);
//  		  回滚事务
	dataSourceTransactionManager.rollback(transaction);

```

