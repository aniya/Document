## 1.导入依赖

```xml
        <dependency>
            <groupId>org.springframework.session</groupId>
            <artifactId>spring-session-data-redis</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-redis</artifactId>
        </dependency>
```

## 2.配置Redis序列化器

## 3.添加注解

`@EnableRedisHttpSession(flushMode = FlushMode.IMMEDIATE)`