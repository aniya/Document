## [启动报错 PathVariable annotation was empty on param 0.]

解决方案：@PathVariable没有写value参数。

```java
    @GetMapping("/v1/admin/{account}")
    Result<SysAdminInfoDto> getInfoByAccount(@PathVariable String account);
```

改为

```java
    @GetMapping("/v1/admin/{account}")
    Result<SysAdminInfoDto> getInfoByAccount(@PathVariable(value = "account") String account);
```



## [启动报错 Consider defining a bean of type 'com.bili.system.feign.AdminFeignClient' in your configuration.]

原因： Springboot只会默认扫描他所在的目录下的文件，比如他只会扫描com.ctw.admin下面的文件不会扫描com.ctw.common , 

解决方案:  在启动类上配置扫描的路径

```
@EnableFeignClients("com.bili.*.feign")
@SpringBootApplication(scanBasePackages = {"com.bili.system","com.bili.auth"})
public class BiliAcgAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(BiliAcgAuthApplication.class, args);
    }

}

```

## [Feign调用时读取超时（Read timed out executing GET）解决]

为Feign调用默认的超时时间为一分钟，一分钟接口不能返回就会抛出异常，所以在服务端的yml文件中增加如下配置即可解决：

```yaml
# feign调用超时时间配置
feign:
  client:
    config:
      default:
        connectTimeout: 10000
        readTimeout: 600000
```

## [Feign调用请求添加请求头，给上游或者下游服务请求时携带请求头]

注册一个请求拦截器，在请求之前将请求头放在远程调用里面上

```java
package com.bili.system.config;

import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author cheng ting wei
 * @Date 2023/6/2 11:20
 */
@Configuration
public class FeignConfig {

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = null;
            if (attr != null) {
                request = attr.getRequest();
                //    同步请求头数据
                String authorization = request.getHeader("Authorization");
                // 给新请求同步了老请求
                requestTemplate.header("Authorization", authorization);
            }
        };
    }

}
```

