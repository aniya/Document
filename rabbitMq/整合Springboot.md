# 整合Springboot

## 1.声明一个队列

```java
    @Bean
    public Queue messageQueue(){
        return new Queue("test.queue",false,false,false);
    }
```

主要有4个参数构造函数为

```java
    public Queue(String name, boolean durable, boolean exclusive, boolean autoDelete) {
        this(name, durable, exclusive, autoDelete, (Map)null);
    }
```

> name 队列名称
>
> durable 是否持久化
>
> exclusive 是否独享
>
> autoDelete 是否自动删除

## 2.声明交换机

```java
    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange("test.exchange",false,false);
    }
```

> 名称，是否持久化，是否自动删除

## 3.绑定队列和交换机

```java
    @Bean
    public Binding queueBindExchange(){
        return BindingBuilder.bind(messageQueue()).to(directExchange())
                .with("test");
    }
```

> 第一个参数是队列
>
> 第二个参数 交换机
>
> 第三个参数 路由键

## 4.发送消息确认

在发送消息后确认消息有没有发送成功

配置文件中添加

```yaml
#    配置消息确认机制
    publisher-confirm-type: correlated
```

自定义RabbitTemplate

```java
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory){
        RabbitTemplate rabbitTemplate = new RabbitTemplate();
//        设置连接工厂
        rabbitTemplate.setConnectionFactory(connectionFactory);
//        开启 强制性确认
        rabbitTemplate.setMandatory(true);
//        生产者发送消息后发送消息
        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            logger.info("消息相关发送成功{},{},{}",correlationData,ack,cause);
        });
//      生产者发送消息后，消息到达队列失败
        rabbitTemplate.setReturnCallback((message, replyCode, replyText, exchange, routingKey) -> {
            logger.info("消息发送失败{}，{}，{}，{}，{}",message,replyCode,replyText,exchange,routingKey);
        });
        return rabbitTemplate;
    }
```

## 5.消息持久化

```java
Message message = MessageBuilder.withBody("message".getBytes())
                .setDeliveryMode(MessageDeliveryMode.PERSISTENT)
                .build();
```

在发送消息的时候可以对消息进行持久化

> 想要消息持久化的前提是，存储消息的队列持久化，否则队列失效消息也会失效

## 6.消费者 消息 确认机制

消费者的确认消费机制有三种：None、Auto、Manual

**None**：不进行确认消息，也就是消费者发送任何反馈信息给MQ服务端；

**Auto**：消费者自动确认消费。消费者处理该消息后，需要发送一个自动的ack反馈信息给MQ服务端，之后该消息从MQ的队列中移除掉。其底层的实现逻辑是由RabbitMQ内置的相关组件实现自动发送确认反馈信息。

**Manual**：人为手动确认消费机制。消费者处理该消息后，需要手动地“以代码的形式”发送给一个ack反馈信息给MQ服务端。

### 设置消息确认配置

```yaml
    listener:
      simple:
        retry:
          enabled: true     # 开启消费者重试机制
          max-attempts: 5 #最大重置数
          max-interval: 1200000ms #重试最大时间间隔（单位毫秒）
          initial-interval: 5000ms #重试间隔时间（单位毫秒）
          multiplier: 2 #间隔时间乘子，间隔时间*乘子=下一次的间隔时间，最大不能超过设置的最大间隔时间
        acknowledge-mode: manual
        prefetch: 1
```

```java
@Component
public class ListenerConfig {

    @RabbitListener(queues = "test.queue")
    @RabbitHandler
    public void getMsg(Message msg, Channel channel) throws IOException {
        try {
            channel.basicAck(msg.getMessageProperties().getDeliveryTag(),false);
        } catch (IOException e) {
            channel.basicNack(msg.getMessageProperties().getDeliveryTag(),false,true);
        }
        System.out.println(msg);
    }


}
```

>  消息拒绝后消息会重新会到队列，进行重试

`channel.basicAck()` 签收

```java
void basicAck(long deliveryTag, boolean multiple) throws IOException;
```

`channel.basicNack`拒绝签收

```java
void basicNack(long deliveryTag, boolean multiple, boolean requeue)
            throws IOException;
```

如果第三个参数为True，则消息会重新回到queue，broker会重新发送消息

## 7.Customer 限流机制

#### 1.设置消费者为手动消息确认

```yaml
        acknowledge-mode: manual
```

#### 2.设置prefetch的数量

```yaml
prefetch: 1
```



数量为1，每一次拉去消息的数量，只有确认之后才会继续拉去消费

数量为10，直接拉去十条来消费，全部确认之后才会继续拉去消费

## 8.消息过期

**死信队列** 和普通队列一样，只是添加了一个过期时间ttl

```java
   @Bean
    public Queue ttlQueue(){
        HashMap<String, Object> map = new HashMap<>();
        map.put("x-message-ttl",100000);
        return new Queue("test.ttl.queue",true,false,true,map);
    }
```

> 设置x-message-ttl ，单位为毫秒 ms

当死信队列过期时，会将所有的消息全部过期

**消息过期** 可以单独给消息设置过期时间

```java
MessagePostProcessor processor = message -> {
//                设置过期时间
            message.getMessageProperties().setExpiration("5000");
            return message;
        };
        rabbitTemplate.convertAndSend("test.ttl.exchange","test.ttl","123",processor);
```

如果设置过期时间的消息再死信队列中，那么过期时间以短的为主

> **消息过期后，只有消息再队列的顶端才会被判断是否过期**

如果有10条消息再100秒的死信队列，在发送一条过期消息为5秒的消息，那么如果前面的10条没有被过期或者消费，后面的5秒消息就不会过期

## 9.死信队列

消息成为死信的三种情况

> 1.队列达到最大长度
>
> 2.消费者拒收消息，并且不把消息放回原来的消息队列重新投送，requeue= false
>
> 3.消息过期

队列绑定死信交换机

`x-dead-letter-exchange` 和 `x-dead-letter-routing-key`

死信队列消息变成死信之后，消息会通过设置的死信交换机发送到其他的队列来消费

流程：

消息 --> 交换机 --> 死信队列 ---> 消息过期或拒收--> 根据设置的交换机和路由键将消息发送到新的队列消费



1，设置正常交换机，死信队列

```java
//    死信队列声明
    @Bean
    public Queue deadQueue(){
        HashMap<String, Object> map = new HashMap<>();
//        死信队列交换机名称
        map.put("x-dead-letter-exchange","dead.exchange");
        map.put("x-dead-letter-routing-key","dead");
        map.put("x-message-ttl",10000);
        map.put("x-max-length",10000)
        return new Queue("dead.queue",true,false,true,map);
    }

    //    死信队列的交换机
    @Bean
    public DirectExchange deadQueueExchange(){
        return new DirectExchange("dead.queue.exchange",true,true);
    }

    //    交换机绑定
    @Bean
    public Binding deadQueueBinding(){
        return BindingBuilder.bind(deadQueue()).to(deadQueueExchange())
                .with("dead.queue.route");
    }

```

2,设置死信交换机和正常队列

```java
//    死信交换机
    @Bean
    public DirectExchange deadExchange(){
        return new DirectExchange("dead.exchange",true,true);
    }
//    正常队列
    @Bean
    public Queue deadTrueQueue(){
        return new Queue("dead.true.queue",true,false,true);
    }
//    交换机绑定
    @Bean
    public Binding deadTrueBinding(){
        return BindingBuilder.bind(deadTrueQueue()).to(deadExchange())
                .with("dead");
    }
```

## 10.延迟队列

安装延时队列插件后开启延时队列功能

```java
@Configuration
public class RabbitMqConfig {

    public static final String DELAYED_EXCHANGE = "delayed_exchange";
    public static final String ORDER_EXPIRE_QUEUE = "order_expire_queue";
    public static final String EXCHANGE_TYPE = "x-delayed-message";

    @Bean
    public Queue orderExpireQueue() {
        return new Queue(ORDER_EXPIRE_QUEUE,true,false,false);
    }

    @Bean
    public Exchange orderExpireExchange() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        return new CustomExchange(DELAYED_EXCHANGE, EXCHANGE_TYPE, true, false, args);
    }

    @Bean
    Binding binding() {
        return BindingBuilder.bind(orderExpireQueue())
                .to(orderExpireExchange()).with(ORDER_EXPIRE_QUEUE).noargs();
    }
}

```

发送消息

```java
        rabbitTemplate.convertAndSend(RabbitMqConfig.DELAYED_EXCHANGE
                , RabbitMqConfig.ORDER_EXPIRE_QUEUE
                , entity.getOrderNo()
                , msg -> {
                    //设置发送消息时的延迟时长
                    msg.getMessageProperties().setDelay(300000);
                    return msg;
                });

```



​	
