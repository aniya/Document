## Docker 安装Rabbitmq插件

**1，下载插件**

**2，将文件复制到Rabbitmq的插件目录**

```shell
docker cp /home/rabbitmq_delayed_message_exchange-3.8.9-0199d11c.ez rabbitmq:/plugins
```

**3，启用插件，并重启Rabbitmq**

```shell
rabbitmq-plugins enable rabbitmq_delayed_message_exchange
```

> 插件不需要加版本号