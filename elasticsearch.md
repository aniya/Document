# ElasticSearch工作台

## 安装IK分词器

1.解压

2.在elasticsearch下面的plugins目录下创建ik文件夹

3.将文件复制进去，重启

## 文档操作

### 索引库操作

mapping属性

> type : 数据类型
>
> index: 是否索引
>
> analyzer : 分词器
>
> properties：子字段

type属性

> 字符串：text，keyword
>
> 数字：long，integer，short，byte，double，float
>
> 布尔：boolean
>
> 日期：date
>
> 对象：Object

##### 创建索引库

```json
PUT /article
{
  "mappings": {
    "properties": {
      "id": {
        "type": "long"
      },
      "title": {
        "type": "text",
        "analyzer": "ik_max_word"
      },
      "approved_time": {
        "type": "date",
        "index": true
      },
      "create_time": {
        "type": "date",
        "index": false
      },
      "like_number": {
        "type": "long"
      },
      "comment_number": {
        "type": "long"
      },
      "view_number": {
        "type": "long"
      },
      "is_original": {
        "type": "boolean"
      }
    }
  }
}

```

> "format": "yyyy-MM-dd HH:mm:ss" 日期类型需要添加格式化

##### 删除索引库

```json
DELETE /article
```

##### 修改索引库

> ElasticSearch不允许修改索引库，但可以添加新的字段

```json
PUT /article/_mapping
{
  "properties":{
    "coin_number":{
      "type":"long"
    }
  }
}
```

##### 查询索引库

```json
GET /article
```

### 文档操作

##### 插入文档

```json
PUT /article/_doc/1
{
  "id":1,
  "approved_time":"2018-04-12 12:32:00",
  "coin_number":1053,
  "comment_number":63432,
  "create_time":"2018-04-05 12:32:00",
  "is_original":true,
  "like_number":1200,
  "title":"Elasticsearch8.0新特性概览",
  "view_number":"412321"
}
```

> 插入文档需要指定id，也就是_doc/1，如果没有指定他会随机生成

> 在插入日期是，需要值创建索引是提前添加format格式化，可以添加多种格式化

##### 查询文档

```json
GET /article/_doc/1
```

##### 删除文档

```json
 DELETE /article/_doc/1
```

##### 修改文档

全量修改，删除旧文档，添加新文档

```json
POST /article/_doc/1
{
  "is_original":false
}
```

局部修改，值修改部分属性

```json
POST /article/_update/1
{
  "doc": {
    "is_original":false
  }
}
```

## 文档查询

### match 模糊查询

### term 精确查询

### range 范围查询

### geo_bounding_box 地理查询 范围

```json
GET /indexName/_search
{
    "query":{
        "geo_bounding_box":{
            "FILED":{
                "top_left":{
                    "lat":12,
                    "lon":21
                },
                "bottom_right":{
                    "lat":30.9,
                    "lon":121.7
                }
            }
        }
    }
}
```

### geo_distance 地理查询，距离查询

>  距离中心点的距离

```json
GET /indexName/_search
{
    "query":{
        "geo_distance":{
			"distance":"15km",
            "FILED":"21.21,213.21"
        }
    }
}
```

### function_source 复合查询，算分

### bool 复合查询

> must : 必须匹配每一个子查询，类似与
>
> should：选择性匹配查询，类似或
>
> must_not: 必须不匹配，不参与算分
>
> filter:必须匹配，不参与算分



or查询

```json
GET /article/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "bool": {
            "must": [
              {
                "match": {
                  "types": "1"
                }
              },
              {
                "match": {
                  "types": "2"
                }
              }
            ]
          }
        },
        {
          "match": {
            "title": "信"
          }
        }
      ]
    }
  }
}
```

